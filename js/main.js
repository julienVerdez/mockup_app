const svgns = "http://www.w3.org/2000/svg";
var formeId = 0;
var drawArea = document.getElementById("svgOne");
var formSelected = "";

var shape = "rect";

var Xdown;
var Ydown;

var Xup;
var Yup;

function makeShape(shapeChoice) {
    shape = shapeChoice;
    //drawArea.removeEventListener('click', selectSquare, true);
    drawArea.addEventListener('mousedown', handleMousedownDraw, true);
    drawArea.addEventListener('mouseup', handleMouseupDraw, true);
}

function handleMousedownDraw(e) {
    Xdown = e.offsetX;
    Ydown = e.offsetY;
    createSquare();
    drawArea.addEventListener('mousemove', sizingSquare, true)
}

function handleMouseupDraw(e) {
    Xup = e.offsetX;
    Yup = e.offsetY;
    drawArea.removeEventListener('mousemove', sizingSquare, true);
    formeId++;
}

function sizingSquare(e) {
    switch (shape) {
        case 'rect':
            if (e.offsetX < Xdown && e.offsetY < Ydown) {
                document.getElementById('forme-' + formeId).setAttributeNS(null, 'x', e.offsetX)
                document.getElementById('forme-' + formeId).setAttributeNS(null, 'y', e.offsetY)
                document.getElementById('forme-' + formeId).setAttributeNS(null, 'height', Ydown - e.offsetY)
                document.getElementById('forme-' + formeId).setAttributeNS(null, 'width', Xdown - e.offsetX)
            }
            else if (e.offsetX > Xdown && e.offsetY < Ydown) {
                document.getElementById('forme-' + formeId).setAttributeNS(null, 'x', Xdown)
                document.getElementById('forme-' + formeId).setAttributeNS(null, 'y', e.offsetY)
                document.getElementById('forme-' + formeId).setAttributeNS(null, 'height', Ydown - e.offsetY)
                document.getElementById('forme-' + formeId).setAttributeNS(null, 'width', e.offsetX - Xdown)
            }
            else if (e.offsetY > Ydown && e.offsetX > Xdown) {
                document.getElementById('forme-' + formeId).setAttributeNS(null, 'height', e.offsetY - Ydown)
                document.getElementById('forme-' + formeId).setAttributeNS(null, 'width', e.offsetX - Xdown)
            }
            else if (e.offsetX < Xdown && e.offsetY > Ydown) {
                document.getElementById('forme-' + formeId).setAttributeNS(null, 'x', e.offsetX)
                document.getElementById('forme-' + formeId).setAttributeNS(null, 'y', Ydown)
                document.getElementById('forme-' + formeId).setAttributeNS(null, 'height', e.offsetY - Ydown)
                document.getElementById('forme-' + formeId).setAttributeNS(null, 'width', Xdown - e.offsetX)
            }
            break;
        case 'circle':
            if (e.offsetX < Xdown && e.offsetY < Ydown) {
                console.log()
                document.getElementById('forme-' + formeId)
                    .setAttributeNS(null, 'r', Math.sqrt(Math.pow((Ydown - e.offsetY), 2) + Math.pow((Xdown - e.offsetX), 2)));
            }
            else if (e.offsetX > Xdown && e.offsetY < Ydown) {
                document.getElementById('forme-' + formeId)
                    .setAttributeNS(null, 'r', Math.sqrt(Math.pow((Ydown - e.offsetY), 2) + Math.pow((e.offsetX - Xdown), 2)));
            }
            else if (e.offsetY > Ydown && e.offsetX > Xdown) {
                document.getElementById('forme-' + formeId)
                    .setAttributeNS(null, 'r', Math.sqrt(Math.pow((e.offsetY - Ydown), 2) + Math.pow((e.offsetX - Xdown), 2)));
            }
            else if (e.offsetX < Xdown && e.offsetY > Ydown) {
                document.getElementById('forme-' + formeId)
                    .setAttributeNS(null, 'r', Math.sqrt(Math.pow((e.offsetY - Ydown), 2) + Math.pow((Xdown - e.offsetX), 2)));
            }
            break;
    }
}

function createSquare() {
    let bgColor = "transparent";
    let borderColor = "#000";
    let borderSize = document.getElementById("borderSize").value;

    formSelected = 'forme-' + formeId;

    switch (shape) {
        case 'rect':
            let rect = document.createElementNS(svgns, 'rect');
            rect.setAttributeNS(null, 'id', 'forme-' + formeId);
            rect.setAttributeNS(null, 'class', 'forme');
            rect.setAttributeNS(null, 'x', Xdown);
            rect.setAttributeNS(null, 'y', Ydown);
            rect.setAttributeNS(null, 'height', "1");
            rect.setAttributeNS(null, 'width', "1");
            rect.setAttributeNS(null, 'fill', bgColor);
            rect.setAttributeNS(null, 'stroke', borderColor);
            rect.setAttributeNS(null, 'stroke-width', borderSize);
            rect.setAttributeNS(null, 'onclick', 'select(' + formeId + ')');

            drawArea.appendChild(rect);
            break;
        case 'circle':
            let circle = document.createElementNS(svgns, 'circle');
            circle.setAttributeNS(null, 'id', 'forme-' + formeId);
            circle.setAttributeNS(null, 'cx', Xdown);
            circle.setAttributeNS(null, 'cy', Ydown);
            circle.setAttributeNS(null, 'r', 1);
            circle.setAttributeNS(null, 'fill', bgColor);
            circle.setAttributeNS(null, 'stroke', borderColor);
            circle.setAttributeNS(null, 'stroke-width', borderSize);
            circle.setAttributeNS(null, 'onclick', 'select(' + formeId + ')');

            drawArea.appendChild(circle);
            break;
    }

}

function selectShape() {
    drawArea.removeEventListener('mousedown', handleMousedowntest, true);
    drawArea.removeEventListener('mouseup', handleMouseuptest, true);
}

function select(id) {
    console.log(formSelected);
    formSelected = "forme-" + id;
    console.log(formSelected);
}

function handleChangeBordersizeForme(size) {
    if (formSelected != "")
        document.getElementById(formSelected).setAttributeNS(null, 'stroke-width', size);
}

function handleChangeBorderColorForme(color) {
    if (formSelected != "")
        document.getElementById(formSelected).setAttributeNS(null, 'stroke', color);
}

function handleChangeBgColorForme(color) {
    if (formSelected != "")
        document.getElementById(formSelected).setAttributeNS(null, 'fill', color);
}
